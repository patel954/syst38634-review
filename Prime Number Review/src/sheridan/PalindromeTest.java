package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid value for palindrome", palindrome.isPalindrome( "anna" ));
	}
	
	@Test
	public void testIsPalindromeNegative() {
		assertFalse("Invalid value for palindrome", palindrome.isPalindrome( "Anna has a lamb" ));
	}
	
	@Test
	public void testIsPalindromeBoundaryIn () {
		assertTrue("Invalid value for palindrome", palindrome.isPalindrome("Aa"));
	}
	
	@Test
	public void testIsPalindromeBoundaryOut () {
		assertFalse("Invalid value for palindrome", palindrome.isPalindrome("racer car"));
	}

}